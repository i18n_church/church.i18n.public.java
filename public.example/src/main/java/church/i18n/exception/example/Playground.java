/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.exception.example;

import static church.i18n.processing.security.policy.SecurityLevel.PERSONAL_INFORMATION;
import static church.i18n.processing.security.policy.SecurityLevel.PUBLIC;
import static church.i18n.processing.security.policy.SecurityLevel.SYSTEM_EXTERNAL;
import static church.i18n.processing.security.policy.SecurityLevel.SYSTEM_INTERNAL;
import static church.i18n.processing.status.ClientError.BAD_REQUEST;
import static church.i18n.processing.validation.Validator.when;

import church.i18n.processing.config.DefaultProcessingExceptionConfig;
import church.i18n.processing.config.ProcessingExceptionConfig;
import church.i18n.processing.exception.ProcessingException;
import church.i18n.processing.logger.LogLevel;
import church.i18n.processing.logger.LogLevelLogMapper;
import church.i18n.processing.logger.LogMapper;
import church.i18n.processing.message.ContextInfo;
import church.i18n.processing.message.ContextValue;
import church.i18n.processing.message.MessageSeverity;
import church.i18n.processing.message.MessageStatus;
import church.i18n.processing.message.ProcessingMessage;
import church.i18n.processing.message.ValueType;
import church.i18n.processing.security.policy.SecurityLevel;
import church.i18n.processing.status.ClientError;
import church.i18n.processing.storage.MessageStorage;
import church.i18n.processing.storage.ProcessingIdProvider;
import church.i18n.processing.storage.ThreadLocalStorage;
import church.i18n.processing.storage.ThreadNameProcessingIdProvider;
import church.i18n.resources.bundles.PolyglotMultiResourceBundle;
import church.i18n.response.domain.Response;
import church.i18n.response.jackson.ResponseModelModule;
import church.i18n.response.mapper.BaseResponseMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

/**
 * This playground contains examples from real world situations and how this library tries to solve
 * them
 */
@SuppressWarnings("ALL")
public class Playground {

  private static final Logger log = LoggerFactory.getLogger(Playground.class);

  private static final boolean realExample = false;

  private static final ProcessingIdProvider idProvider = () -> "f4a6d28b-2acb-40db-8c14-710fc25157e9";
  private static final ThreadLocalStorage messageStorage = new ThreadLocalStorage(idProvider);
  private static final ProcessingExceptionConfig config = DefaultProcessingExceptionConfig.builder()
      .build();

  public void exampleNewProcessingException() {
    throw new ProcessingException("example-err-1");
  }

  public void exampleNewProcessingExceptionWithParameters() {
    String email = "info@@i18n.church";

    String EMAIL_REGEXP = "^[^@\\s]+@[^@\\s.]+\\.[^@.\\s]+$";
    Pattern EMAIL_PATTERN = Pattern.compile(EMAIL_REGEXP);
    when(email, Objects::isNull)
        .buildException("err-format-email", email)
        .withHelpUri("https://tools.ietf.org/html/rfc5322")
        .withSecurityLevel(SecurityLevel.SYSTEM_EXTERNAL)
        .withLogLevel(LogLevel.ERROR)
        .withStatus(BAD_REQUEST)
        .addContextInfo(
            when(email, Objects::nonNull)
                .buildContextInfo("email-value")
                .withContext("wrong@@email.com", ValueType.STRING)
                .withHelp("^[^@\\s]+@[^@\\s\\.]+\\.[^@\\.\\s]+$", ValueType.REGEXP)
                .withMessage("err-format-email-ctx", email, 7)
                .withSecurityLevel(SecurityLevel.SYSTEM_INTERNAL)
                .build()
        )
        .throwException();
    when(email, Objects::isNull)
        .buildProcessingMessage("err-format-email", email)
        .withHelpUri("https://tools.ietf.org/html/rfc5322")
        .withSecurityLevel(SecurityLevel.SYSTEM_EXTERNAL)
        .addContextInfo(
            ContextInfo.of("email-value")
                .withContext("wrong@@email.com", ValueType.STRING)
                .withHelp("^[^@\\s]+@[^@\\s\\.]+\\.[^@\\.\\s]+$", ValueType.REGEXP)
                .withMessage("err-format-email-ctx", email, 7)
                .withSecurityLevel(SecurityLevel.SYSTEM_INTERNAL)
                .build()
        )
        .addToMessageStorage(messageStorage);

    if (realExample) {
      throw new IllegalArgumentException("The e-mail address has incorrect format. The value: "
          + "was: " + email);
    } else {
      ProcessingException processingException =
          new ProcessingException("err-format-email", email)
              .withMessageType(MessageSeverity.HIGH)
              .withStatus(BAD_REQUEST)
              .withSecurityLevel(SecurityLevel.SYSTEM_EXTERNAL)
              .withLogLevel(LogLevel.ERROR)
              .withHelpUri("https://tools.ietf.org/html/rfc5322")
              .addContextInfo(
                  ContextInfo.of("email-value")
                      .withContext("wrong@@email.com", ValueType.STRING)
                      .withHelp("^[^@\\s]+@[^@\\s\\.]+\\.[^@\\.\\s]+$", ValueType.REGEXP)
                      .withMessage("err-format-email-ctx", email, 7)
                      .withSecurityLevel(SecurityLevel.PERSONAL_INFORMATION)
                      .build()
              );

      LogMapper logMapper = new LogLevelLogMapper((infoSecurityPolicy, config) -> true, config);
      try {
        throw processingException;
      } catch (ProcessingException pe) {
        logMapper.log(pe);
      }

      //throw processingException;
    }
  }

  public void localization() {
    ProcessingExceptionConfig config = DefaultProcessingExceptionConfig.builder()
        .withExposeSecurityPolicies(Set.of(PUBLIC, SYSTEM_EXTERNAL, PERSONAL_INFORMATION))
        .withLogSecurityPolicies(Set.of(PUBLIC, SYSTEM_INTERNAL, SYSTEM_EXTERNAL))
        .build();
    BaseResponseMapper mapper = BaseResponseMapper.builder()
        .withBundle(new PolyglotMultiResourceBundle("i18n.example-errors"))
        .withConfig(config)
        .build();

    String email = "info@@i18n.church";
    ProcessingException processingException =
        new ProcessingException("err-format-email", email)
            .withMessageType(MessageStatus.ERROR)
            .withStatus(BAD_REQUEST)
            .withSecurityLevel(SecurityLevel.SYSTEM_EXTERNAL)
            .withLogLevel(LogLevel.ERROR)
            .withHelpUri("https://tools.ietf.org/html/rfc5322")
            .addContextInfo(
                ContextInfo.of("email-value")
                    .withContext(email, ValueType.STRING)
                    .withHelp("^[^@\\s]+@[^@\\s\\.]+\\.[^@\\.\\s]+$", ValueType.REGEXP)
                    .withMessage("err-format-email-ctx", email, 6)
                    .withSecurityLevel(SecurityLevel.PERSONAL_INFORMATION)
                    .build()
            );
    Locale locale = new Locale("sk", "SK");
    Response<?> slovakResponse = mapper.map(processingException, List.of(locale));
    Response<?> defaultResponse = mapper.map(processingException, List.of());
    System.out.println(slovakResponse);
    System.out.println(defaultResponse);
    mapper.getLogMapper().log(processingException);
  }

  public void formValidationErrors() throws JsonProcessingException {
    ProcessingExceptionConfig config = DefaultProcessingExceptionConfig.builder()
        .withExposeSecurityPolicies(Set.of(SecurityLevel.values()))
        .withLogSecurityPolicies(Set.of(SecurityLevel.values()))
        .build();
    BaseResponseMapper mapper = BaseResponseMapper.builder()
        .withBundle(new PolyglotMultiResourceBundle("i18n.example-errors"))
        .withMessageStorage(messageStorage)
        .withProcessingIdProvider(idProvider)
        .withConfig(config)
        .build();
    ObjectMapper objectMapper = new Jackson2ObjectMapperBuilder()
        .modules(new ResponseModelModule())
        .build();

    String username = "";
    boolean strongPassword = false;
    int age = 10;
    int MIN_AGE = 14;
    when(username, String::isBlank)
        .buildProcessingMessage("err-username-empty")
        .addToMessageStorage(messageStorage);
    when(!strongPassword)
        .buildProcessingMessage("err-weak-password")
        .addToMessageStorage(messageStorage);
    when(age, a -> a < MIN_AGE)
        .buildProcessingMessage("err-age-too-young", MIN_AGE)
        .addContextInfo(ContextInfo.of("age", new ContextValue(age, ValueType.INTEGER)))
        .addToMessageStorage(messageStorage);

    List<ProcessingMessage> processingMessages = messageStorage
        .get(idProvider.getProcessingId());

    Optional<ProcessingException> exceptionOptional = when(processingMessages,
        Predicate.not(List::isEmpty))
        .buildException("err-form-contains-errors")
        .build();

    when(processingMessages, Predicate.not(List::isEmpty))
        .buildException("err-form-contains-errors")
        .withStatus(ClientError.BAD_REQUEST)
        .throwException();

    Response<?> map = mapper.map(exceptionOptional.get(), List.of());
    System.out.println(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(map));

  }

  public void messageStorage() {
    ProcessingIdProvider idProvider = new ThreadNameProcessingIdProvider();
    MessageStorage messageStorage = new ThreadLocalStorage(idProvider);
    String total = "";//ByteUtils.humanized(1_073_741_824L);
    String available = "";//ByteUtils.humanized(513_802_240L);
    String threshold = "";//ByteUtils.humanized(524_288_000L);

    boolean runningOutOfSpace = true;
    when(runningOutOfSpace)
        .buildProcessingMessage("disk-space", available, total)
        .withMessageType(MessageStatus.WARNING)
        .withHelpUri("https://somestorage.com/kb/disk-space-threshold")
        .addContextInfo(
            ContextInfo.of("disk-space-total").withContext(total)
                .withHelp("https://somestorage.com/plans/buy-storage", "URL")
                .withMessage("disk-space-total", total).build(),
            ContextInfo.of("disk-space-available").withContext(available)
                .withHelp("https://somestorage.com/plans/buy-storage", "URL")
                .withMessage("disk-space-available", available).build(),
            ContextInfo.of("disk-space-threshold").withContext(threshold)
                .withHelp("https://somestorage.com/profile/settings/disk-space-alert", "URL")
                .withMessage("disk-space-threshold", threshold).build()
        )
        .addToMessageStorage(messageStorage);

  }

  public void exampleBuildProcessingException() {
    throw new ProcessingException("example-err-1");
  }

  public void mapperExample() {
    ProcessingExceptionConfig config = DefaultProcessingExceptionConfig.builder()
        .withExposeSecurityPolicies(Set.of(PUBLIC, SYSTEM_EXTERNAL, PERSONAL_INFORMATION))
        .withLogSecurityPolicies(Set.of(PUBLIC, SYSTEM_INTERNAL, SYSTEM_EXTERNAL))
        .build();
    BaseResponseMapper mapper = BaseResponseMapper.builder()
        .withBundle(new PolyglotMultiResourceBundle("i18n.example-errors"))
        .withConfig(config)
        .withProcessingIdProvider(() -> UUID.randomUUID().toString())
        .build();
    String email = "info@@i18n.church";
    ProcessingException processingException =
        new ProcessingException("err-format-email", email)
            .withMessageType(MessageStatus.ERROR)
            .withStatus(BAD_REQUEST)
            .withSecurityLevel(SecurityLevel.SYSTEM_EXTERNAL)
            .withLogLevel(LogLevel.ERROR)
            .withHelpUri("https://tools.ietf.org/html/rfc5322")
            .addContextInfo(
                ContextInfo.of("email-value")
                    .withContext(email, ValueType.STRING)
                    .withHelp("^[^@\\s]+@[^@\\s\\.]+\\.[^@\\.\\s]+$", ValueType.REGEXP)
                    .withMessage("err-format-email-ctx", email, 7)
                    .withSecurityLevel(SecurityLevel.PERSONAL_INFORMATION)
                    .build()
            );
    Locale locale = new Locale("sk", "SK");
    Response<?> response = mapper.map(processingException, List.of());
    System.out.println(response);
  }

  public static void main(final String[] args) throws JsonProcessingException {
    new Playground().formValidationErrors();
//    new Playground().localization();
//    new Playground().mapperExample();
  }

  private void emailValidation() {
    String email = "wrong@@email.com";
    throw new ProcessingException("err-format-email", email);
  }

  private class IAProcessingException extends ProcessingException {

    private static final long serialVersionUID = -8908262402718334452L;

    public IAProcessingException(@NotNull final String code,
        @Nullable final Object... params) {
      super(code, params);
    }

    public IAProcessingException(
        @NotNull final ProcessingMessage message) {
      super(message);
    }

    public IAProcessingException(
        @NotNull final ProcessingMessage message,
        @NotNull final Throwable cause) {
      super(message, cause);
    }
  }
}
