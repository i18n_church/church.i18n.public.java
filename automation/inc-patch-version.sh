#!/bin/bash
#
# Copyright (c) 2024 Juraj Jurčo
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
# associated documentation files (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge, publish, distribute,
# sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or
# substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
# NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

set -e

# Change the location of execution to the content root
cd "$(dirname "$0")/.."
# echo "Current working directory is: $(pwd)"

# Ensure the file is provided as an argument
if [[ $# -ne 1 ]]; then
    echo "Usage: $0 <build.gradle.kts file>"
    exit 1
fi

gradle_file="$1"

# Read the current version line
current_version_line=$(grep -E 'rootProject\.version\s*=\s*".*"' "$gradle_file")

if [[ -z "$current_version_line" ]]; then
    echo "Version not found in $gradle_file"
    exit 1
fi

# Extract the current version
current_version=$(echo "$current_version_line" | grep -oE '[0-9]+\.[0-9]+\.[0-9]+')

if [[ -z "$current_version" ]]; then
    echo "Failed to extract the version number"
    exit 1
fi

# Increment the patch version
IFS='.' read -r major minor patch <<< "$current_version"
new_version="$major.$minor.$((patch + 1))"

# Update the version in the file
sed -i.bak "s/rootProject\.version = \"$current_version\"/rootProject.version = \"$new_version\"/" "$gradle_file"

# Cleanup backup file (optional)
rm -f "${gradle_file}.bak"

# Print the updated version
echo "Version updated to $new_version in $gradle_file"
