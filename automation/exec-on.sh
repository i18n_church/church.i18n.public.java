#!/bin/sh
#
# Copyright (c) 2025 Juraj Jurčo
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
# associated documentation files (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge, publish, distribute,
# sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or
# substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
# NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

set -e

# Change the location of execution to the content root
cd "$(dirname "$0")/.."
echo "Current working directory is: $(pwd)"
ls -la

# Define valid parameters and their associated folders
PARAMETER_MAP="-testable:processing.exception resources.bundles response response.model.jackson response.spring response.spring.webmvc
-submodules:processing.exception resources.bundles response response.model.jackson response.spring.webmvc response.spring spring.webmvc.platform
-mavenPublish:processing.exception resources.bundles response response.model.jackson response.spring.webmvc response.spring
-modules:processing.exception resources.bundles response response.model.jackson response.spring.webmvc response.spring spring.webmvc.platform ."

# Function to get folders based on parameter
get_folders() {
    echo "$PARAMETER_MAP" | grep "^$1:" | cut -d':' -f2
}

# Check if at least one parameter is provided
if [ "$#" -lt 2 ]; then
    echo "Error: Missing required parameter or action."
    echo "Usage: $0 <parameter> <action>"
    echo "Valid parameters are: -testable, -submodules, -modules, -mavenPublish"
    exit 1
fi

# Validate the first parameter
PARAMETER="$1"
FOLDERS=$(get_folders "$PARAMETER")

if [ -z "$FOLDERS" ]; then
    echo "Error: Invalid parameter '$PARAMETER'."
    echo "Valid parameters are: -testable, -submodules, -modules"
    exit 1
fi

# Shift to remove the parameter and keep the action
shift

# Ensure an action is provided
if [ "$#" -lt 1 ]; then
    echo "Error: Missing action after the parameter."
    echo "Usage: $0 <parameter> <action>"
    exit 1
fi

# Execute the action on each folder
for FOLDER in $FOLDERS; do
    if [ -d "$FOLDER" ]; then
        echo "Executing action in folder: $FOLDER"
        (cd "$FOLDER" && pwd && ls -la && git status && "$@") || {
            echo "Error: Failed to execute action in $FOLDER."
            exit 1
        }
    else
        echo "Warning: Folder $FOLDER does not exist. Skipping."
    fi
done

echo "Action completed successfully."