#!/bin/bash
#
# Copyright (c) 2024 Juraj Jurčo
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
# associated documentation files (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge, publish, distribute,
# sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or
# substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
# NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

CONFIG_FILE="gpg-key.config"
QUIET=false

# Check for -q (quiet mode)
if [[ "$1" == "-q" ]]; then
  QUIET=true
fi

# Confirmation message
if [ "$QUIET" = false ]; then
  echo -e "\n\033[1;31m*** WARNING ***\033[0m"
  echo -e "1. \033[1;33mStore your GPG PASSPHRASE securely!\033[0m"
  echo -e "2. \033[1;33mDon't forget to update the generated GPG key in BitBucket.\033[0m\n"
  echo "Do you want to continue? [y/N]"
  read -r CONFIRMATION
  if [[ "$CONFIRMATION" != "y" && "$CONFIRMATION" != "Y" ]]; then
    echo "Aborting script."
    exit 1
  fi
fi

echo "Create a config"
PASSPHRASE=$(openssl rand -base64 33)
cat > $CONFIG_FILE <<EOF
%no-protection
Key-Type: EDDSA
Key-Curve: Ed25519
Name-Real: Juraj Jurco
Name-Email: jjurco.sk@gmail.com
Name-Comment: church.i18n.public
Expire-Date: 1y
Passphrase: $PASSPHRASE
%commit
%echo done
EOF

#1. Generate a key
gpg --batch --generate-key "$CONFIG_FILE"

# Verify the generated key
echo -e "\n\033[1;32mGenerated GPG Keys:\033[0m"
gpg --list-keys

# Print private key (only if not in quiet mode)
if [ "$QUIET" = false ]; then
  echo -e "\n\033[1;33mExporting Private Key (Base64 Encoded):\033[0m"

  # Fetch key ID (40letter ID) - sorting from newest to oldest and picking just the first one.
  KEY_ID=$(gpg --list-keys --with-colons | grep '^pub' | sort -t: -k6,6r | head -n1 | awk -F: '{print $5}')

  if [ -n "$KEY_ID" ]; then
    # Export and encode private key - we need only content, without start/end sections and without checksum. Signing plugin doesn't like that
    # -----BEGIN PGP PRIVATE KEY BLOCK-----
    #
    #<Multiline content of the key>
    #=Q4bm <=It's a checksum of the key. Gradle can't handle it, we need only the content of the key.>
    #-----END PGP PRIVATE KEY BLOCK-----

    # sed '/^-----/d; /^[=]/d'
    #   ^----- matches and removes lines starting with ----- (headers and footers).
    #   /^[=]/d removes lines that begin with =, which include the Base64 CRC24 checksum.
    # tr -d '\n'
    #   Removes all newlines to concatenate the entire key into a single string.
    GPG_SIGNING_KEY=$(gpg --armor --export-secret-keys $KEY_ID | sed '/^-----/d; /^[=]/d' | tr -d '\n')
    #2. upload certificate to the public server
    gpg --export "$KEY_ID" | curl -T - https://keys.openpgp.org

    echo -e "\033[1;33mDon't forget to update the generated GPG key in BitBucket.\033[0m\n"
    echo -e "\n\033[1;32mEnvironment configuration:\033[0m"
    echo -e "\033[1;33mGPG_SIGNING_PASSWORD: $PASSPHRASE\033[0m"
    echo -e "\033[1;33mGPG_SIGNING_KEY: $GPG_SIGNING_KEY\033[0m"

    echo -e "\n\033[1;31m*** Store this private key securely! ***\033[0m"
  else
    echo "Error: Unable to determine GPG key ID."
  fi
fi

echo -e "\n\033[1;32mScript completed successfully.\033[0m"
