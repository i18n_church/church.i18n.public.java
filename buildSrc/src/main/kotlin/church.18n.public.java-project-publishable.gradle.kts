/*
 * Copyright (c) 2025 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

plugins {
    id("java")
    id("maven-publish")
    id("signing")
}

group = "church.i18n"
version = project.version
description = project.description

java {
    withJavadocJar()
    withSourcesJar()
}

publishing {
    publications {
        create<MavenPublication>("churchI18n") {
            from(components["java"])

            groupId = project.group.toString()
            artifactId = project.name
            version = project.version.toString()

            versionMapping {
                usage("java-api") {
                    fromResolutionOf("runtimeClasspath")
                }
                usage("java-runtime") {
                    fromResolutionResult()
                }
            }
            pom {
                name.set(project.name)
                description.set(project.description)
                url.set("https://bitbucket.org/i18n_church/${project.name}")

                licenses {
                    license {
                        name.set("The MIT License")
                        url.set("https://opensource.org/licenses/MIT")
                    }
                }
                developers {
                    developer {
                        id.set("jianghongtiao")
                        name.set("Juraj Jurčo")
                        email.set("jjurco.sk@gmail.com")
                    }
                }
                scm {
                    connection.set("scm:git:ssh://git@bitbucket.org/i18n_church/${project.name}.git")
                    developerConnection.set("scm:git:ssh://git@bitbucket.org/i18n_church/${project.name}.git")
                    url.set("https://bitbucket.org/i18n_church/${project.name}")
                }
                issueManagement {
                    system.set("BitBucket")
                    url.set("https://bitbucket.org/i18n_church/${project.name}/issues")
                }
            }

            repositories {
                maven {
                    name = "sonatype"

                    url = uri("https://oss.sonatype.org/service/local/staging/deploy/maven2")
                    credentials {
                        username = getPropertyOrEnv(prop = "sonatypeUsername", env = "SONATYPE_USERNAME")
                        password = getPropertyOrEnv(prop = "sonatypePassword", env = "SONATYPE_PASSWORD")
                    }
                }
            }
        }
    }
}

signing {
    val signingKey: String? = getPropertyOrEnv(prop = "signing.key", env = "GPG_SIGNING_KEY")
    val signingPassword: String? = getPropertyOrEnv(prop = "signing.password", env = "GPG_SIGNING_PASSWORD")
    useInMemoryPgpKeys(signingKey, signingPassword)
    sign(publishing.publications)
}

allprojects {
    afterEvaluate {
        println("Listing all available publications for project: ${project.name}")
        project.publishing.publications.forEach { publication ->
            println("------------------------------------")
            println(">> Name: ${publication.name}")
            println(">> Class: ${publication::class.java}")

            if (publication is MavenPublication) {
                println(">> MavenPublication: ${publication.groupId}:${publication.artifactId}:${publication.version}")
                publication.artifacts.forEach { artifact ->
                    println(">>\tArtifact: ${artifact.classifier}:${artifact.extension}")
                }
            }
        }
        println("------------------------------------")
    }
}
