/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

plugins {
    id("java-platform")
    id("maven-publish")
    id("signing")
}

description = """This BOM project centralizes dependency management and version alignment for 
    |modules focused on exception handling. It supports a generic, universal model for 
    |abstracting and managing exceptions, ensuring consistency and maintainability.
    |""".trimMargin()

javaPlatform {
    allowDependencies()
}

dependencies {
    constraints {
        api(project(":processing.exception"))
        api(project(":resources.bundles"))
        api(project(":response"))
        api(project(":response.model.jackson"))
        api(project(":response.spring"))
        api(project(":response.spring.webmvc"))

        api(libs.slf4j.api.platform)
        api(libs.jetbrains.annotations.platform)
    }
}

publishing {
    publications {
        create<MavenPublication>("churchI18nPlatform") {
            from(components["javaPlatform"])

            groupId = project.group.toString()
            artifactId = "spring-webmvc-platform"
            version = project.version.toString()

            pom {
                name.set(project.name)
                description.set(project.description)
                url.set("https://bitbucket.org/i18n_church/${project.name}")

                licenses {
                    license {
                        name.set("The MIT License")
                        url.set("https://opensource.org/licenses/MIT")
                    }
                }
                developers {
                    developer {
                        id.set("jianghongtiao")
                        name.set("Juraj Jurčo")
                        email.set("jjurco.sk@gmail.com")
                    }
                }
                scm {
                    connection.set("scm:git:ssh://git@bitbucket.org/i18n_church/${project.name}.git")
                    developerConnection.set("scm:git:ssh://git@bitbucket.org/i18n_church/${project.name}.git")
                    url.set("https://bitbucket.org/i18n_church/${project.name}")
                }
                issueManagement {
                    system.set("BitBucket")
                    url.set("https://bitbucket.org/i18n_church/${project.name}/issues")
                }
            }

            repositories {
                maven {
                    url = uri("https://oss.sonatype.org/service/local/staging/deploy/maven2")
                    credentials {
                        username = findProperty("sonatypeUsername") as String? ?: System.getenv("sonatypeUsername")
                        password = findProperty("sonatypePassword") as String? ?: System.getenv("sonatypePassword")
                    }
                }
            }
        }
    }
}

signing {
    sign(publishing.publications["churchI18nPlatform"])
}